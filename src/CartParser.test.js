import CartParser from './CartParser';
import cart from '../samples/cart.json';
import { parse } from 'querystring';

let parser;

beforeEach(() => {
    parser = new CartParser();
});

describe('CartParser - unit tests', () => {
    // Add your unit tests here.
    //1
    it('validate should return error on incorrect header', () => {
        //given
        const header = 'A, B, C';
        //when
        const validErrors = parser.validate(header);
        //then
        expect(validErrors.length).toBe(3);
    });
    //2
    it('validate should return error on incorrect number of cells in a row', () => {
        //given
        const text = `Product name,Price,Quantity\n
		Mollis consequat,9.00\n`
            //when
        const validErrors = parser.validate(text);
        //then
        expect(validErrors.length).toBe(1);
    });
    //3
    it('validate should return error on empty cell of type string', () => {
        //given
        const text = `Product name,Price,Quantity\n
		,9.00,2\n`
            //when
        const validErrors = parser.validate(text);
        //then
        expect(validErrors.length).toBe(1);
    });
    //4
    it('validate should return error on negative number', () => {
        //given
        const text = `Product name,Price,Quantity\n
		Mollis consequat,-9.00,-2`
            //when
        const validErrors = parser.validate(text);
        //then
        expect(validErrors.length).toBe(2);
    });
    //5
    it('validate should return error if number cell contain other data but number', () => {
        //given
        const text = `Product name,Price,Quantity\n
		Mollis consequat,9.00,bug`
            //when
        const validErrors = parser.validate(text);
        //then
        expect(validErrors.length).toBe(1);
    });
    //6
    it('validate should return empty array if data is valid', () => {
        //given
        const text = `Product name,Price,Quantity\n
		Mollis consequat,9.00,2`
            //when
        const validErrors = parser.validate(text);
        //then
        expect(validErrors).toEqual([]);
    });
    //7
    it('parseLine should return valid property names', () => {
        //given
        const text = `Mollis consequat,9.00,2`
        const keys = parser.schema.columns.map(c => c.key);
        keys.push('id');
        //when
        const parsedLine = parser.parseLine(text);
        //then
        expect(Object.keys(parsedLine)).toEqual(keys);
    });
    //8
    it('parseLine should return valid values', () => {
        //given
        const name = 'Mollis consequat';
        const price = 9.00;
        const quantity = 2;
        const text = `${name},${price},${quantity}`
            //when
        const parsedLine = parser.parseLine(text);
        //then
        expect(parsedLine.name).toBe(name);
        expect(parsedLine.price).toBe(price);
        expect(parsedLine.quantity).toBe(quantity);
    });
    //9
    it('parseLine should return NaN if numbers are not specified', () => {
        //given
        const text = ``;
        //when
        const parsedLine = parser.parseLine(text);
        //then
        expect(isNaN(parsedLine.price)).toBeTruthy();
        expect(isNaN(parsedLine.quality)).toBeTruthy();
    });
});

describe('CartParser - integration test', () => {
    // Add your integration test here.
    it('parse should return correct value on example', () => {
        //given
        const path = 'samples/cart.csv';
        //when
        const result = parser.parse(path);
        //then
        const resultItemsNoId = result.items.map(c => delete c.id);
        const cartItemsNoId = cart.items.map(c => delete c.id);
        expect(resultItemsNoId).toEqual(cartItemsNoId);
        expect(result.total).toEqual(cart.total);
    });
});